
#include <gtest/gtest.h>

#include "../list.h"

// Needed to include these libraries for my list of integers
#include <iostream>
#include <list>


TEST(Container_List,Size_functionality) {


  using namespace mylib;


  List list { 1,2,3,4 };



  EXPECT_EQ(4,list.size());


}




