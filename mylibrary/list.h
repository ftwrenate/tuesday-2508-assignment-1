#ifndef LIST_H
#define LIST_H


#include <initializer_list>
#include <memory>

namespace mylib {


struct ListElement
{

    int numberOfElements;
    using size_type  = std::size_t;

    std::shared_ptr<ListElement> _next;
     // TODO make constructors here
    //ListElement();
    ListElement(){_next=nullptr;};
    ListElement(size_type s);
    ~ListElement(); // destructor

};



  class List {
  public:

      // TODO A default constructor that shall not get any sort of values.



    using size_type  = std::size_t;
    using value_type = int;

    List( std::initializer_list<value_type> list );



    size_type size() const;


  private:
    size_type _size;
  }; // END class List

}  // END namespace mylib

#endif // LIST_H
