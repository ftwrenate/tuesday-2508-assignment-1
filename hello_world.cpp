// mylib
#include "mylibrary/functions.h"
#include "mylibrary/list.h"

// stl
#include <iostream>
#include <stdexcept>
#include <list>

// The one and only "allowed" MACRO -- MACROs are NO NO NO NOOOOOOOOOOOOO !!!! 
// NOT EVEN SLIGHTLY TYPE SAFE - JUST LOOK AT WHAT THIS DOES (... OR DOES NOT)
#define UNUSED(ARG) (void)ARG;

struct ListElement
{
    ListElement* nextElement;
    ListElement* previousElement;
    int numberOfElements;
};
int main(int argc, char** argv) try {
  UNUSED(argc)
  UNUSED(argv)

  mylib::helloWorld( "... anywho ..." );


  mylib::List list{2,3,4,5};




/*-------------------------------------------------
 * Implement my list
 * code created by ftwrenate
 * ------------------------------------------------
 */

  int myList[] = {51,2,7,33,21}; // Creates my list
    std::list<int> myIntegers (myList,myList+5);
      myIntegers.push_back(0); // Start with 0 at the beginning
      myIntegers.push_front(0); // End with 0 at the end
      myIntegers.insert(++++myIntegers.begin(),500); // The number 500 is at the third position
    std::cout << "My list contains of these integers: \n";

    /*Iterator pointing to the first number in myIntegers.
     * If there is no value the iterator shall not be referenced
     * (Iterators --> begin(), end())
      */
    for (std::list<int>::iterator myIterator=myIntegers.begin(); myIterator != myIntegers.end(); ++myIterator)
      std::cout << ' ' << *myIterator;

    std::cout << '\n';

    std::cout << "After sorting my list of integers: \n";
    myIntegers.sort(); // Sorts the list in ascending order
    for (std::list<int>::iterator myIterator=myIntegers.begin(); myIterator != myIntegers.end(); ++myIterator)
      std::cout << ' ' << *myIterator;

    std::cout << '\n';

        if(myIntegers.empty())
        {
            std::cout << "The list is empty...";
        }

  return 0;
}


catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}

